import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './_pages/login/login.component';
import { HomeComponent } from './_pages/home/home.component';
import { RegistroComponent } from './_pages/registro/registro.component';
import { RegistroBanquitoComponent } from './_pages/registro-banquito/registro-banquito.component';
import { AuthGuard } from './_guards/auth.guard';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegistroComponent },
    { path: 'banco', component: RegistroBanquitoComponent, canActivate: [AuthGuard] },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const Routing = RouterModule.forRoot(appRoutes);