import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroBanquitoComponent } from './registro-banquito.component';

describe('RegistroBanquitoComponent', () => {
  let component: RegistroBanquitoComponent;
  let fixture: ComponentFixture<RegistroBanquitoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroBanquitoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroBanquitoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
