import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AuthGuard } from './_guards/auth.guard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routing }        from './app.routing';
import { AuthService } from './_services/auth.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './_pages/home/home.component';
import { LoginComponent } from './_pages/login/login.component';
import { RegistroComponent } from './_pages/registro/registro.component';
import { HeaderComponent } from './_pages/template/header/header.component';
import { FooterComponent } from './_pages/template/footer/footer.component';
import { RegistroBanquitoComponent } from './_pages/registro-banquito/registro-banquito.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegistroComponent,
    HeaderComponent,
    FooterComponent,
    RegistroBanquitoComponent
  ],
  imports: [
    BrowserModule,
    Routing,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AuthGuard,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
